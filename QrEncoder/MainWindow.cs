﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.Win32;
using ThoughtWorks.QRCode.Codec;
using Xceed.Wpf.Toolkit;

namespace QrEncoder
{
  class MainWindow : Window
  {
    GroupBox config_group;
    Label mode_label;
    Label correction_label;
    Label scale_label;
    Label version_label;
    Label foreground_label;
    Label background_label;
    ComboBox mode_combo;
    ComboBox correction_combo;
    ColorPicker foreground_color_picker;
    ColorPicker background_color_picker;
    IntegerUpDown scale_up_down;
    IntegerUpDown version_up_down;

    GroupBox logo_group;
    TextBox logo_path_box;
    Button  logo_path_button;

    GroupBox data_group;
    TextBox data_box;

    GroupBox preview_group;
    Bitmap qrcode_bitmap;
    System.Windows.Controls.Image qrcode_img;

    GroupBox operation_group;
    Button   save_button;
    Button   generate_button;

    bool qrcode_generated;

    private bool IsGenerated
    {
      get { return qrcode_generated; }
      set { qrcode_generated = value; }
    }

    [STAThread]
    public static void Main()
    {
      Application app = new Application();
      app.Run(new MainWindow());
    }

    public MainWindow()
    {
      WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
      SizeToContent = SizeToContent.Manual;
      Width = 640;
      Height = 500;
      MinWidth = 640;
      MinHeight = 500;

      Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
      Title = string.Format("{0} V{1}.{2}", Properties.Resources.MainWindowTitle, version.Major, version.Minor);

      IsGenerated = false;

      // config
      config_group = new GroupBox();
      config_group.Padding = new Thickness(5);
      config_group.Header = Properties.Resources.ConfigGroupHeader;

      mode_label = new Label();
      mode_label.Content = Properties.Resources.ModeLabelText;

      correction_label = new Label();
      correction_label.Margin = new Thickness(0, 5, 0, 0);
      correction_label.Content = Properties.Resources.CorrectionLabelText;

      scale_label = new Label();
      scale_label.Margin = new Thickness(0, 5, 0, 0);
      scale_label.Content = Properties.Resources.ScaleLabelText;

      version_label = new Label();
      version_label.Margin = new Thickness(0, 5, 0, 0);
      version_label.Content = Properties.Resources.VersionLabelText;

      foreground_label = new Label();
      foreground_label.Margin = new Thickness(0, 5, 0, 0);
      foreground_label.Content = Properties.Resources.ForegroundLabelText;

      background_label = new Label();
      background_label.Margin = new Thickness(0, 5, 0, 0);
      background_label.Content = Properties.Resources.BackgroundLabelText;

      mode_combo = new ComboBox();
      Dictionary<QRCodeEncoder.ENCODE_MODE, string> mode_dic = new Dictionary<QRCodeEncoder.ENCODE_MODE, string>() 
      { 
        {QRCodeEncoder.ENCODE_MODE.BYTE, Properties.Resources.EncodeModeComboByte},
        {QRCodeEncoder.ENCODE_MODE.NUMERIC, Properties.Resources.EncodeModeComboNumeric},
        {QRCodeEncoder.ENCODE_MODE.ALPHA_NUMERIC, Properties.Resources.EncodeModeComboAlphaNumeric}
      };
      mode_combo.ItemsSource = mode_dic;
      mode_combo.SelectedValuePath = "Key";
      mode_combo.DisplayMemberPath = "Value";
      mode_combo.SelectedValue = QRCodeEncoder.ENCODE_MODE.BYTE;

      correction_combo = new ComboBox();
      correction_combo.Margin = new Thickness(0, 5, 0, 0);
      Dictionary<QRCodeEncoder.ERROR_CORRECTION, string> correction_dic = new Dictionary<QRCodeEncoder.ERROR_CORRECTION, string>()
      {
        {QRCodeEncoder.ERROR_CORRECTION.L, Properties.Resources.CorrectionComboLevelL},
        {QRCodeEncoder.ERROR_CORRECTION.M, Properties.Resources.CorrectionComboLevelM},
        {QRCodeEncoder.ERROR_CORRECTION.Q, Properties.Resources.CorrectionComboLevelQ},
        {QRCodeEncoder.ERROR_CORRECTION.H, Properties.Resources.CorrectionComboLevelH}
      };
      correction_combo.ItemsSource = correction_dic;
      correction_combo.SelectedValuePath = "Key";
      correction_combo.DisplayMemberPath = "Value";
      correction_combo.SelectedValue = QRCodeEncoder.ERROR_CORRECTION.L;

      scale_up_down = new IntegerUpDown();
      scale_up_down.Margin = new Thickness(0, 5, 0, 0);
      scale_up_down.Minimum = 10;
      scale_up_down.Maximum = 100;
      scale_up_down.Value = 10;

      version_up_down = new IntegerUpDown();
      version_up_down.Margin = new Thickness(0, 5, 0, 0);
      version_up_down.Minimum = 0;
      version_up_down.Maximum = 40;
      version_up_down.Value = 0;

      foreground_color_picker = new ColorPicker();
      foreground_color_picker.Margin = new Thickness(0, 5, 0, 0);
      foreground_color_picker.SelectedColor = System.Windows.Media.Colors.Black;

      background_color_picker = new ColorPicker();
      background_color_picker.Margin = new Thickness(0, 5, 0, 0);
      background_color_picker.SelectedColor = System.Windows.Media.Colors.Transparent;

      // logo
      logo_group = new GroupBox();
      logo_group.Margin = new Thickness(0, 5, 0, 0);
      logo_group.Padding = new Thickness(5);
      logo_group.Header = Properties.Resources.LogoGroupHeader;

      logo_path_box = new TextBox();

      logo_path_button = new Button();
      logo_path_button.MinWidth = 50;
      logo_path_button.Margin = new Thickness(5, 0, 0, 0);
      logo_path_button.Content = Properties.Resources.LogoButtonText;
      logo_path_button.Click += OnLogoBrowse;

      // data
      data_group = new GroupBox();
      data_group.Margin = new Thickness(0, 5, 0, 0);
      data_group.Padding = new Thickness(5);
      data_group.Header = Properties.Resources.DataGroupHeader;

      data_box = new TextBox();
      data_box.HorizontalAlignment = HorizontalAlignment.Stretch;
      data_box.VerticalAlignment = VerticalAlignment.Stretch;
      data_box.TextWrapping = TextWrapping.Wrap;
      data_box.AcceptsReturn = true;
      data_box.MinHeight = 100;
      data_box.MinWidth = 240;
      data_box.MaxWidth = 240;
      data_group.Content = data_box;

      // preview
      preview_group = new GroupBox();
      preview_group.HorizontalAlignment = HorizontalAlignment.Stretch;
      preview_group.VerticalAlignment = VerticalAlignment.Stretch;
      preview_group.Padding = new Thickness(5);
      preview_group.Header = Properties.Resources.PreviewGroupHeader;

      qrcode_img = new System.Windows.Controls.Image();
      qrcode_img.Margin = new Thickness(0, 5, 0, 0);
      qrcode_img.Stretch = Stretch.Uniform;
      qrcode_img.HorizontalAlignment = HorizontalAlignment.Stretch;
      qrcode_img.VerticalAlignment = VerticalAlignment.Stretch;
      preview_group.Content = qrcode_img;

      // operation
      operation_group = new GroupBox();
      operation_group.Padding = new Thickness(5);
      operation_group.Margin = new Thickness(0, 5, 0, 0);
      operation_group.Header = Properties.Resources.OperationGroupHeader;

      save_button = new Button();
      save_button.IsEnabled = IsGenerated;
      save_button.MinWidth = 80;
      save_button.Margin = new Thickness(5, 0, 0, 0);
      save_button.HorizontalAlignment = HorizontalAlignment.Right;
      save_button.VerticalAlignment = VerticalAlignment.Top;
      save_button.Content = Properties.Resources.SaveButtonText;
      save_button.Click += OnSave;

      generate_button = new Button();
      generate_button.MinWidth = 80;
      generate_button.Margin = new Thickness(5, 0, 0, 0);
      generate_button.HorizontalAlignment = HorizontalAlignment.Right;
      generate_button.VerticalAlignment = VerticalAlignment.Top;
      generate_button.Content = Properties.Resources.GenerateButtonText;
      generate_button.Click += OnGenerate;

      // layout
      DockPanel logo_panel = new DockPanel();
      logo_panel.HorizontalAlignment = HorizontalAlignment.Stretch;
      logo_panel.VerticalAlignment = VerticalAlignment.Stretch;
      DockPanel.SetDock(logo_path_box, Dock.Left);
      DockPanel.SetDock(logo_path_button, Dock.Right);
      logo_panel.Children.Add(logo_path_button);
      logo_panel.Children.Add(logo_path_box);
      logo_group.Content = logo_panel;

      Grid config_grid = new Grid();
      config_grid.HorizontalAlignment = HorizontalAlignment.Left;
      config_grid.VerticalAlignment = VerticalAlignment.Top;
      config_grid.RowDefinitions.Add(new RowDefinition()); // mode
      config_grid.RowDefinitions.Add(new RowDefinition()); // correction
      config_grid.RowDefinitions.Add(new RowDefinition()); // scale
      config_grid.RowDefinitions.Add(new RowDefinition()); // version
      config_grid.RowDefinitions.Add(new RowDefinition()); // foreground
      config_grid.RowDefinitions.Add(new RowDefinition()); // background
      config_grid.RowDefinitions.Add(new RowDefinition()); // logo
      config_grid.ColumnDefinitions.Add(new ColumnDefinition()); // label
      config_grid.ColumnDefinitions.Add(new ColumnDefinition()); // editor

      Grid.SetRow(mode_label, 0);           // mode
      Grid.SetRow(mode_combo, 0);
      Grid.SetColumn(mode_label, 0);
      Grid.SetColumn(mode_combo, 1);
      Grid.SetRow(correction_label, 1);     // correction
      Grid.SetRow(correction_combo, 1);
      Grid.SetColumn(correction_label, 0);
      Grid.SetColumn(correction_combo, 1);
      Grid.SetRow(scale_label, 2);          // scale
      Grid.SetRow(scale_up_down, 2);
      Grid.SetColumn(scale_label, 0);
      Grid.SetColumn(scale_up_down, 1);
      Grid.SetRow(version_label, 3);        // version
      Grid.SetRow(version_up_down, 3);
      Grid.SetColumn(version_label, 0);
      Grid.SetColumn(version_up_down, 1);
      Grid.SetRow(foreground_label, 4);     // foreground
      Grid.SetRow(foreground_color_picker, 4);
      Grid.SetColumn(foreground_label, 0);
      Grid.SetColumn(foreground_color_picker, 1);
      Grid.SetRow(background_label, 5);     // background
      Grid.SetRow(background_color_picker, 5);
      Grid.SetColumn(background_label, 0);
      Grid.SetColumn(background_color_picker, 1);
      Grid.SetRow(logo_group, 6);           // logo
      Grid.SetColumn(logo_group, 0);
      Grid.SetColumnSpan(logo_group, 2);

      config_grid.Children.Add(mode_label);
      config_grid.Children.Add(mode_combo);
      config_grid.Children.Add(correction_label);
      config_grid.Children.Add(correction_combo);
      config_grid.Children.Add(scale_label);
      config_grid.Children.Add(scale_up_down);
      config_grid.Children.Add(version_label);
      config_grid.Children.Add(version_up_down);
      config_grid.Children.Add(foreground_label);
      config_grid.Children.Add(foreground_color_picker);
      config_grid.Children.Add(background_label);
      config_grid.Children.Add(background_color_picker);
      config_grid.Children.Add(logo_group);
      config_group.Content = config_grid;

      StackPanel operation_panel = new StackPanel();
      operation_panel.HorizontalAlignment = HorizontalAlignment.Right;
      operation_panel.Orientation = Orientation.Horizontal;
      operation_panel.Children.Add(save_button);
      operation_panel.Children.Add(generate_button);
      operation_group.Content = operation_panel;

      DockPanel left_panel = new DockPanel();
      left_panel.HorizontalAlignment = HorizontalAlignment.Stretch;
      left_panel.VerticalAlignment = VerticalAlignment.Stretch;
      DockPanel.SetDock(config_group, Dock.Top);
      DockPanel.SetDock(data_group, Dock.Bottom);
      left_panel.Children.Add(config_group);
      left_panel.Children.Add(data_group);

      DockPanel right_panel = new DockPanel();
      right_panel.Margin = new Thickness(5, 0, 0, 0);
      right_panel.HorizontalAlignment = HorizontalAlignment.Stretch;
      right_panel.VerticalAlignment = VerticalAlignment.Stretch;
      DockPanel.SetDock(preview_group, Dock.Top);
      DockPanel.SetDock(operation_group, Dock.Bottom);
      right_panel.Children.Add(operation_group);
      right_panel.Children.Add(preview_group);

      DockPanel main_panel = new DockPanel();
      main_panel.Margin = new Thickness(5);
      main_panel.HorizontalAlignment = HorizontalAlignment.Stretch;
      main_panel.VerticalAlignment = VerticalAlignment.Stretch;
      DockPanel.SetDock(left_panel, Dock.Left);
      DockPanel.SetDock(right_panel, Dock.Right);
      main_panel.Children.Add(left_panel);
      main_panel.Children.Add(right_panel);

      Content = main_panel;
    }

    private void OnLogoBrowse(object sender, System.EventArgs e)
    {
      OpenFileDialog dialog = new OpenFileDialog();
      dialog.Title = Properties.Resources.LogoOpenDialogTitle;
      dialog.Filter = Properties.Resources.LogoOpenFilter;
      if (dialog.ShowDialog() == true)
        logo_path_box.Text = dialog.FileName;
    }

    private void OnSave(object sender, System.EventArgs e)
    {
      SaveFileDialog dialog = new SaveFileDialog();
      dialog.Title = Properties.Resources.CodeSaveDialogTitle;
      dialog.Filter = Properties.Resources.CodeSaveFilter;
      if(dialog.ShowDialog() == true)
        qrcode_bitmap.Save(dialog.FileName);
    }

    private void OnGenerate(object sender, System.EventArgs e)
    {
      string data = data_box.Text;
      if (data.Length == 0)
      {
        string title = Properties.Resources.GenerateErrorTitile;
        string text = Properties.Resources.GenerateErrorEmpty;
        System.Windows.MessageBox.Show(this, text, title);
        return;
      }

      // set encoder parameters
      QRCodeEncoder encoder = new QRCodeEncoder();
      encoder.QRCodeEncodeMode = (QRCodeEncoder.ENCODE_MODE)mode_combo.SelectedValue;
      encoder.QRCodeErrorCorrect = (QRCodeEncoder.ERROR_CORRECTION)correction_combo.SelectedValue;
      encoder.QRCodeForegroundColor = System.Drawing.Color.FromArgb(foreground_color_picker.SelectedColor.Value.A,
                                                                    foreground_color_picker.SelectedColor.Value.R,
                                                                    foreground_color_picker.SelectedColor.Value.G,
                                                                    foreground_color_picker.SelectedColor.Value.B);
      encoder.QRCodeBackgroundColor = System.Drawing.Color.FromArgb(background_color_picker.SelectedColor.Value.A,
                                                                    background_color_picker.SelectedColor.Value.R,
                                                                    background_color_picker.SelectedColor.Value.G,
                                                                    background_color_picker.SelectedColor.Value.B);

      if (version_up_down.Value.HasValue)
        encoder.QRCodeVersion = version_up_down.Value.Value;
      else
        encoder.QRCodeVersion = 0;

      if (scale_up_down.Value.HasValue)
        encoder.QRCodeScale = scale_up_down.Value.Value;
      else
        encoder.QRCodeScale = 10;

      // generate qrcode bitmap
      try
      {
        qrcode_bitmap = encoder.Encode(data, Encoding.UTF8);
      }
      catch(System.IndexOutOfRangeException)
      {
        string title = Properties.Resources.GenerateErrorTitile;
        string text = Properties.Resources.GenerateErrorOutOfRange;
        System.Windows.MessageBox.Show(this, text, title);
        return;
      }

      // logo
      if(!logo_path_box.Text.Trim().Equals(string.Empty))
      {
        try
        {
          Bitmap logo = new Bitmap(logo_path_box.Text.Trim());
          Bitmap logo_stretched = new Bitmap(logo, qrcode_bitmap.Width / 5, qrcode_bitmap.Height / 5);
          Graphics graphics = Graphics.FromImage(qrcode_bitmap);
          int x = qrcode_bitmap.Width / 2 - logo_stretched.Width / 2;
          int y = qrcode_bitmap.Height / 2 - logo_stretched.Height / 2;
          graphics.DrawImage(logo_stretched, x, y);
        }
        catch(System.IO.FileNotFoundException)
        {
          string title = Properties.Resources.GenerateErrorTitile;
          string text = Properties.Resources.GenerateErrorLogoNotFound;
          System.Windows.MessageBox.Show(this, text, title);
        }
      }

      // display
      qrcode_img.Source = BitmapToBitmapSource(qrcode_bitmap);
      IsGenerated = true;

      save_button.IsEnabled = IsGenerated;
    }

    private BitmapSource BitmapToBitmapSource(Bitmap bitmap)
    {
      if (bitmap == null)
        return null;
      return Imaging.CreateBitmapSourceFromHBitmap(bitmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
    }
  }
}
